#include <stdlib.h>
#include <stdint.h>

#include <wayland-util.h>

struct remote_stream;

typedef int (*remote_stream_handler_func_t)(struct remote_stream *stream,
					    size_t remain, void *data);

struct remote_stream_handler {
	remote_stream_handler_func_t func;
	void *data;
};

struct remote_stream {
	uint32_t tag, header;
	int len, total;
	int fd, protocol_fd;
	uint32_t tag_handler_count;
	struct remote_stream_handler tag_handler[10];
	struct remote_stream_handler *handler;
	struct wl_event_source *source;
	struct wl_list block_queue;
};

struct remote_stream_block {
	struct wl_list link;
	void *data;
	size_t size;
	size_t written;
	uint32_t tag;
};

int
remote_stream_write(struct remote_stream *stream);

int
remote_stream_read(struct remote_stream *stream);

void
remote_stream_handle_tag(struct remote_stream *stream, uint32_t tag,
			 remote_stream_handler_func_t func, void *data);

void
remote_stream_queue_block(struct remote_stream *stream,
			  struct remote_stream_block *block,
			  void *data, size_t size);
