/*
 * Copyright © 2012 Intel Corporation
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of the copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.  The copyright holders make
 * no representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/uio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "compositor.h"
#include "../shared/os-compatibility.h"

#include "remote-stream.h"
#include "remote-server-protocol.h"


struct remote_server {
	struct weston_compositor *compositor;
	int sock;
	struct wl_event_source *sock_source;
	struct wl_global *global;
};

struct remote_client {
	struct remote_server *server;
	int protocol_fd[2];
	struct wl_event_source *protocol_source;
	struct wl_client *client;
	struct wl_listener destroy_listener;
	struct wl_resource *remote_resource;

	struct remote_stream *stream; 
	struct remote_stream_block protocol_stream_block;
	struct remote_stream_block keymap_stream_block;
};

static int
buffer_data_tag_handler(struct remote_stream *stream,
			size_t remain, void *data)
{
	struct wl_buffer *buffer = data;
	char *buffer_data;
	int len;

	buffer_data = wl_shm_buffer_get_data(buffer);
	len = read(stream->fd, buffer_data + stream->len, remain);
	if (len < 0) {
		weston_log("buffer data read failed: %m\n");
		return -1;
	}
	stream->len += len;

	return len;
}

static int
stream_handle_event(int fd, uint32_t mask, void *data)
{
	struct remote_client *client = data;

	if (mask & (WL_EVENT_HANGUP | WL_EVENT_ERROR)) {
		fprintf(stderr, "stream_handle_event: hangup\n");
		wl_client_destroy(client->client);
		return 1;
	}

	remote_stream_read(client->stream);

	return 1;
}

static int
protocol_handle_event(int fd, uint32_t mask, void *data)
{
	struct remote_client *client = data;
	struct remote_stream_block *block;
	char buffer[4096];
	int len;

	if (mask & (WL_EVENT_HANGUP | WL_EVENT_ERROR))
		fprintf(stderr, "protocol_handle_event: hangup\n");

	len = read(fd, buffer, sizeof buffer);

	block = &client->protocol_stream_block;
	wl_list_insert(&client->stream->block_queue, &block->link);
	block->tag = 0;
	block->written = 0;
	block->size = len;
	block->data = buffer;

	remote_stream_write(client->stream);

	return 1;
}

static int
protocol_tag_handler(struct remote_stream *stream, size_t remain, void *data)
{
	struct remote_client *client = data;
	char buffer[4096];
	int len;

	if (remain > sizeof buffer)
		len = sizeof buffer;
	else
		len = remain;

	len = read(stream->fd, buffer, len);
	if (len < 0)
		return -1;
	stream->len += len;
	write(client->protocol_fd[0], buffer, len);

	return len;
}

static struct remote_stream *
remote_stream_accept(struct remote_server *server,
		     struct remote_client *client)
{
	struct weston_compositor *compositor = server->compositor;
	struct wl_event_loop *loop =
		wl_display_get_event_loop(compositor->wl_display);
	struct remote_stream *stream;
	struct sockaddr_in name;
	socklen_t length;
	int fd;

	length = sizeof name;
	fd = accept4(server->sock,
		     (struct sockaddr *) &name, &length, SOCK_CLOEXEC);
	if (fd < 0) {
		weston_log("failed to accept: %m\n");
		return NULL;
	}

	stream = malloc(sizeof *stream);
	if (stream == NULL)
		return NULL;

	memset(stream, 0, sizeof *stream);
	stream->tag_handler[0].func = protocol_tag_handler;
	stream->tag_handler[0].data = client;
	stream->tag = 1;
	stream->tag_handler_count = 1;
	stream->fd = fd;
	wl_list_init(&stream->block_queue);

	stream->source =
		wl_event_loop_add_fd(loop, stream->fd, WL_EVENT_READABLE,
				     stream_handle_event, client);
	if (stream->source == NULL) {
		weston_log("failed to create remote client: %m\n");
		close(fd);
		free(stream);
		return NULL;
	}

	return stream;
}

static void
remote_stream_destroy(struct remote_stream *stream)
{
	wl_event_source_remove(stream->source);
	close(stream->fd);
	free(stream);
}

static void
remote_client_destroy(struct wl_listener *listener, void *data)
{
	struct remote_client *client =
		container_of(listener, struct remote_client, destroy_listener);

	/* FIXME: Clean up objects */

	remote_stream_destroy(client->stream);

	wl_event_source_remove(client->protocol_source);
	close(client->protocol_fd[0]);
	close(client->protocol_fd[1]);

	free(client);
}

static int
sock_handle_event(int fd, uint32_t mask, void *data)
{
	struct remote_server *server = data;
	struct weston_compositor *compositor = server->compositor;
	struct remote_client *client;
	struct wl_event_loop *loop =
		wl_display_get_event_loop(compositor->wl_display);

	client = malloc(sizeof *client);
	if (client == NULL) {
		weston_log("failed to create remote-server client: %m\n");
		return 1;
	}

	memset(client, 0, sizeof *client);
	client->server = server;
	client->stream = remote_stream_accept(server, client);
	if (client->stream == NULL) {
		free(client);
		return 1;
	}

	if (os_socketpair_cloexec(AF_UNIX, SOCK_STREAM, 0,
				  client->protocol_fd) < 0) {
		weston_log("failed to create socketpair: %m\n");
		return 1;
	}

	client->protocol_source =
		wl_event_loop_add_fd(loop, client->protocol_fd[0],
				     WL_EVENT_READABLE,
				     protocol_handle_event, client);
	if (client->protocol_source == NULL) {
		weston_log("failed to create remote client source: %m\n");
		free(client);
		return 1;
	}

	client->client = wl_client_create(compositor->wl_display,
					  client->protocol_fd[1]);

	client->destroy_listener.notify = remote_client_destroy;
	wl_client_add_destroy_listener(client->client,
				       &client->destroy_listener);

	weston_log("remote client connected (%p)\n", client->client);

	return 1;
}

static int
remote_try_send_keymap(struct wl_client *client,
		       struct weston_seat *seat, struct wl_resource *cr)
{
	struct wl_listener *listener;
	struct remote_client *rc;

	listener = wl_client_get_destroy_listener(client,
						  remote_client_destroy);
	if (listener == NULL)
		return -1;

	rc = container_of(listener, struct remote_client, destroy_listener);

	remote_stream_queue_block(rc->stream, &rc->keymap_stream_block,
				  seat->xkb_info.keymap_area,
				  seat->xkb_info.keymap_size);
	remote_send_keymap(rc->remote_resource, cr, 
			   WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1,
			   rc->keymap_stream_block.tag,
			   seat->xkb_info.keymap_size);

	return 0;
}

static int
make_socket(uint16_t port)
{
	int sock, one = 1;
	struct sockaddr_in name;

	sock = socket(PF_INET, SOCK_STREAM | SOCK_CLOEXEC | SOCK_NONBLOCK, 0);
	if (sock < 0) {
		weston_log("failed to create remote-server socket: %m\n");
		return -1;
	}

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof one);

	name.sin_family = AF_INET;
	name.sin_port = htons(port);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(sock, (struct sockaddr *) &name, sizeof name) < 0) {
		weston_log("failed to bind remote-server socket: %m\n");
		close(sock);
		return -1;
	}

	if (listen(sock, 1) < 0) {
		weston_log("failed to listen on remote-server socket: %m\n");
		close(sock);
		return -1;
	}

	return sock;
}

static void
remote_handle_create_buffer(struct wl_client *client,
			    struct wl_resource *resource, uint32_t id,
			    int32_t width, int32_t height, uint32_t format)
{
	switch (format) {
	case REMOTE_FORMAT_ARGB8888:
	case REMOTE_FORMAT_XRGB8888:
		break;
	default:
		wl_resource_post_error(resource,
				       WL_SHM_ERROR_INVALID_FORMAT,
				       "invalid format");
		return;
	}

	/* FIXME: check integer overflow */
	if (width <= 0 || height <= 0) {
		wl_resource_post_error(resource,
				       WL_SHM_ERROR_INVALID_STRIDE,
				       "invalid width, height or stride (%dx%d)",
				       width, height);
		return;
	}

	/* FIXME: This is going to send spurious buffer.release events
	 * to the remote side... or is it just right? */

	/* FIXME: Get wl_resource and set a destroy listener? */
	wl_shm_buffer_create(client, id, width, height, width * 4, format);
}

static void
remote_handle_update_buffer(struct wl_client *client,
			    struct wl_resource *resource,
			    struct wl_resource *buffer_resource,
			    uint32_t tag,
			    struct wl_resource *region)
{
	struct wl_listener *listener;
	struct remote_client *rc;

	listener = wl_client_get_destroy_listener(client,
						  remote_client_destroy);
	rc = container_of(listener, struct remote_client, destroy_listener);

	remote_stream_handle_tag(rc->stream, tag,
				 buffer_data_tag_handler,
				 buffer_resource->data);
}

static const struct remote_interface remote_implementation = {
	remote_handle_create_buffer,
	remote_handle_update_buffer
};

static void
bind_remote(struct wl_client *client,
	    void *data, uint32_t version, uint32_t id)
{
	struct remove_server *server = data;
	struct wl_listener *listener;
	struct remote_client *rc;

	/* Reject non-remote clients.  This interface only makes sense
	 * for remote clients that can refer to out-of-band objects. */
	listener = wl_client_get_destroy_listener(client,
						  remote_client_destroy);
	if (!listener)
		return;

	rc = container_of(listener, struct remote_client, destroy_listener);
	rc->remote_resource =
		wl_client_add_object(client, &remote_interface,
				     &remote_implementation, id, server);
}

int
module_init(struct weston_compositor *compositor);

WL_EXPORT int
module_init(struct weston_compositor *compositor)
{
	struct remote_server *server;
	struct wl_event_loop *loop =
		wl_display_get_event_loop(compositor->wl_display);
	int port = 2222;

	server = malloc(sizeof *server);
	if (server == NULL)
		return -1;

	server->compositor = compositor;
	server->sock = make_socket(port);
	if (server->sock < 0) {
		free(server);
		return -1;
	}

	server->sock_source =
		wl_event_loop_add_fd(loop, server->sock, WL_EVENT_READABLE,
				     sock_handle_event, server);
	if (server->sock_source == NULL) {
		weston_log("failed to set up remote-server socket source\n");
		free(server);
		return -1;
	}

	server->global = wl_display_add_global(compositor->wl_display,
					       &remote_interface,
					       server, bind_remote);


	compositor->try_send_keymap = remote_try_send_keymap;

	weston_log("remote server listening on port %d\n", port);

	return 0;
}
