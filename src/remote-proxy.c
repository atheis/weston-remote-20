/*
 * Copyright © 2012 Intel Corporation
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of the copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.  The copyright holders make
 * no representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <linux/input.h>
#include <sys/uio.h>
#include <assert.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include <wayland-server.h>
#include <wayland-client.h>
#include "compositor.h"

#include "../shared/os-compatibility.h"

#include "remote-stream.h"
#include "remote-client-protocol.h"

/*
 * - Don't abuse weston_seat, weston_output etc.
 *
 * - Encapsulate fd users in abstract objects?  wl_buffer,
 *   wl_keyboard_map... idea is to keep most of core protocol the
 *   same, but allow the out of band usage to be encapsulated.
 *
 * - Minimize shm update regions.
 *
 * - Surface enter/leave.
 *
 * - Different background image for other compositor (for demos).
 *
 */

struct remote {
	struct weston_compositor *compositor;
	struct wl_listener destroy_listener;
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_list surface_list;
	struct wl_compositor *remote_compositor;
	struct wl_shell *shell;
	struct wl_event_source *source;
	uint32_t event_mask;

	int protocol_fd[2];
	char protocol_buffer[4096];
	struct wl_event_source *protocol_source;
	struct remote_stream *stream;
	struct remote *proxy_remote;

	struct remote_stream_block protocol_stream_block;
};

struct remote_output {
	struct weston_output output;
	struct remote *remote;
	struct wl_output *proxy;
};

struct remote_seat {
	struct wl_seat seat;
	struct remote *remote;
	struct wl_seat *proxy;
	struct wl_pointer *proxy_pointer;
	struct wl_keyboard *proxy_keyboard;
	uint32_t enter_serial;
	struct wl_pointer pointer;
	struct wl_keyboard keyboard;

	int keymap_fd;
	void *keymap_area;
	size_t keymap_size;
};

struct shell_surface {
	struct wl_resource resource;
	struct weston_surface *surface;
};

struct remote_surface {
	struct weston_surface *surface;
	struct wl_surface *proxy;
	struct wl_shell_surface *proxy_shell_surface;

	struct shell_surface *shell_surface;

	struct remote *remote;
	struct wl_buffer *proxy_buffer;
	void *map;
	int width, height;

	uint32_t remote_ping_serial;
	uint32_t subst_ping_serial;

	struct wl_listener destroy_listener;

	const struct wl_surface_interface *interface;
	const struct wl_shell_surface_interface *shell_surface_interface;
	GLuint fbo;

	struct remote_stream_block stream_block;
};

static struct remote_seat *
is_remote_seat(struct wl_resource *resource);

static void
destroy_remote(struct wl_listener *listener, void *data);

static int
keymap_tag_handler(struct remote_stream *stream,
		   size_t remain, void *data)
{
	struct remote_seat *seat = data;
	int len;

	if (stream->len == 0) {
		seat->keymap_size = stream->total;
		seat->keymap_fd =
			os_create_anonymous_file(seat->keymap_size);
		seat->keymap_area = mmap(NULL, seat->keymap_size,
					 PROT_READ | PROT_WRITE,
					 MAP_SHARED,
					 seat->keymap_fd, 0);
	}

	len = read(stream->fd, seat->keymap_area + stream->len, remain);
	if (len < 0)
		return -1;

	stream->len += len;

	return len;
}

static int
stream_data(int fd, uint32_t mask, void *data)
{
	struct remote *remote = data;

	if (mask & (WL_EVENT_HANGUP | WL_EVENT_ERROR)) {
		fprintf(stderr, "stream_data: hangup\n");
		destroy_remote(&remote->destroy_listener, NULL);
		return 1;
	}

	if (mask & WL_EVENT_WRITABLE) {
		remote_stream_write(remote->stream);
		if (wl_list_empty(&remote->stream->block_queue)) {
			wl_event_source_fd_update(remote->source,
						  WL_EVENT_READABLE);
			wl_event_source_fd_update(remote->stream->source,
						  WL_EVENT_READABLE);
		}
	}

	if (mask & WL_EVENT_READABLE)
		remote_stream_read(remote->stream);

	return 1;
}
 
static int
protocol_data(int fd, uint32_t mask, void *data)
{
	struct remote *remote = data;
	struct remote_stream_block *block;
	int len;

	len = read(fd, remote->protocol_buffer,
		   sizeof remote->protocol_buffer);

	block = &remote->protocol_stream_block;
	wl_list_insert(&remote->stream->block_queue, &block->link);
	block->tag = 0;
	block->written = 0;
	block->size = len;
	block->data = remote->protocol_buffer;

	/* FIXME: Try non-blocking write before switching poll masks
	 * around. */
	wl_event_source_fd_update(remote->source, 0);
	wl_event_source_fd_update(remote->stream->source,
				  WL_EVENT_READABLE | WL_EVENT_WRITABLE);

	return 1;
}

static int
protocol_tag_handler(struct remote_stream *stream, size_t remain, void *data)
{
	struct remote *remote = data;
	char buffer[4096];
	int len;

	if (remain > sizeof buffer)
		len = sizeof buffer;
	else
		len = remain;

	len = read(stream->fd, buffer, len);
	stream->len += len;
	write(remote->protocol_fd[0], buffer, len);

	return 1;
}

static struct remote_stream *
remote_stream_connect(struct remote *remote,
		      const char *hostname, int port)
{
	struct wl_event_loop *loop =
		wl_display_get_event_loop(remote->compositor->wl_display);
	struct remote_stream *stream;
	struct sockaddr_in name;
	struct hostent *hostinfo;

	stream = malloc(sizeof *stream);
	if (stream == NULL)
		return NULL;

	memset(stream, 0, sizeof *stream);
	stream->tag_handler[0].func = protocol_tag_handler;
	stream->tag_handler[0].data = remote;
	stream->tag = 1;
	stream->tag_handler_count = 1;
	wl_list_init(&stream->block_queue);

	hostinfo = gethostbyname(hostname);
	if (hostinfo == NULL) {
		weston_log("Unknown host %s.\n", hostname);
		free(stream);
		return NULL;
	}

	name.sin_family = AF_INET;
	name.sin_port = htons(port);
	name.sin_addr = *(struct in_addr *) hostinfo->h_addr;

	stream->fd = socket(PF_INET,
			    SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);
	if (stream->fd < 0) {
		weston_log("Failed to create socket for remote stream: %m.\n");
		free(stream);
		return NULL;
	}
	
	if (connect(stream->fd, (struct sockaddr *) &name, sizeof name) < 0 &&
	    errno != EINPROGRESS) {
		weston_log("Could not connect to remote server: %m.\n");
		return NULL;
	}

	/* FIXME: Poll socket for writability, check for error with
	 * getsockopt for SO_ERROR, level SOL_SOCKET. */

	stream->source =
		wl_event_loop_add_fd(loop, stream->fd,
				     WL_EVENT_READABLE, stream_data, remote);

	return stream;
}

static void
surface_enter(void *data,
	      struct wl_surface *wl_surface, struct wl_output *wl_output)
{
}

static void
surface_leave(void *data,
	      struct wl_surface *wl_surface, struct wl_output *output)
{
}

static const struct wl_surface_listener surface_listener = {
	surface_enter,
	surface_leave
};

static void
shell_surface_handle_ping(void *data, struct wl_shell_surface *shell_surface,
			  uint32_t serial)
{
	struct remote_surface *surface = data;
	struct weston_compositor *compositor = surface->remote->compositor;

	surface->remote_ping_serial = serial;
	surface->subst_ping_serial =
		wl_display_get_serial(compositor->wl_display);
	wl_shell_surface_send_ping(&surface->shell_surface->resource,
				   surface->subst_ping_serial);
}

static void
shell_surface_handle_configure(void *data,
			       struct wl_shell_surface *shell_surface,
			       uint32_t edges, int32_t width, int32_t height)
{
	struct remote_surface *surface = data;

	wl_shell_surface_send_configure(&surface->shell_surface->resource,
					edges, width, height);
}

static void
shell_surface_handle_popup_done(void *data,
				struct wl_shell_surface *shell_surface)
{
}

static const struct wl_shell_surface_listener shell_surface_listener = {
	shell_surface_handle_ping,
	shell_surface_handle_configure,
	shell_surface_handle_popup_done
};

static void
remote_surface_commit(struct remote_surface *surface)
{
	struct remote *remote = surface->remote;
	struct wl_buffer *buffer = surface->surface->buffer;
	struct wl_region *region;
	void *data;
	int size;

	size = surface->width * 4 * surface->height;
	if (buffer && wl_buffer_is_shm(buffer)) {
		data = wl_shm_buffer_get_data(buffer);
		memcpy(surface->map, data, size);
	} else {
		glGenFramebuffers(1, &surface->fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, surface->fbo);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
				       GL_TEXTURE_2D, surface->surface->textures[0], 0);

		glReadPixels(0, 0, surface->width, surface->height,
			     remote->compositor->read_format,
			     GL_UNSIGNED_BYTE, surface->map);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	remote_stream_queue_block(remote->stream,
				  &surface->stream_block, surface->map,
				  surface->width * 4 * surface->height);
	region = wl_compositor_create_region(remote->remote_compositor);
	wl_region_add(region, 0, 0, surface->width, surface->height);
	remote_update_buffer(remote->proxy_remote, surface->proxy_buffer,
			     surface->stream_block.tag, region);
	wl_region_destroy(region);

	wl_surface_damage(surface->proxy,
			  0, 0, surface->width, surface->height);
}

static void
remote_surface_destroy(struct wl_listener *listener, void *data)
{
	struct remote_surface *surface =
		container_of(listener,
			     struct remote_surface, destroy_listener);

	wl_surface_destroy(surface->proxy);
	if (surface->proxy_shell_surface)
		wl_shell_surface_destroy(surface->proxy_shell_surface);

	free(surface);
}

static struct remote_surface *
get_remote_surface(struct wl_resource *resource)
{
	struct wl_listener *listener;

	listener = wl_signal_get(&resource->destroy_signal,
				 remote_surface_destroy);

	if (listener == NULL)
		return NULL;

	return container_of(listener, struct remote_surface, destroy_listener);
}

static void
surface_handle_destroy(struct wl_client *client, struct wl_resource *resource)
{
	struct remote_surface *surface = get_remote_surface(resource);

	surface->interface->destroy(client, resource);
}

static void
remote_surface_attach(struct remote_surface *surface,
		      struct wl_buffer *buffer, int32_t sx, int32_t sy)
{
	struct remote *remote = surface->remote;
	struct weston_compositor *compositor = remote->compositor;
	int stride, size;
	uint32_t format;
	EGLint texture_format;

	if (surface->proxy_buffer != NULL && buffer != NULL &&
	    surface->width == buffer->width &&
	    surface->height == buffer->height)
		return;

	if (surface->proxy_buffer != NULL)
		wl_buffer_destroy(surface->proxy_buffer);

	if (buffer == NULL) {
		wl_surface_attach(surface->proxy, NULL, sx, sy);
		return;
	}

	stride = buffer->width * 4;
	size = stride * buffer->height;

	if (wl_buffer_is_shm(buffer)) {
		/* Remote formats match shm formats */
		format = wl_shm_buffer_get_format(buffer);
	} else if (compositor->query_buffer(compositor->egl_display, buffer,
					    EGL_TEXTURE_FORMAT,
					    &texture_format)) {
		switch (texture_format) {
		case EGL_TEXTURE_RGB:
			format = REMOTE_FORMAT_XRGB8888;
			break;
		case EGL_TEXTURE_RGBA:
			format = REMOTE_FORMAT_ARGB8888;
			break;
		default:
			weston_log("can't forward yuv surfaces\n");
			return;
		}
	} else {
		weston_log("unhandled surface type\n");
		return;
	}

	surface->width = buffer->width;
	surface->height = buffer->height;
	surface->map = malloc(size);

	surface->proxy_buffer =
		remote_create_buffer(remote->proxy_remote,
				     buffer->width, buffer->height, format);

	wl_surface_attach(surface->proxy, surface->proxy_buffer, sx, sy);
}

static void
surface_handle_attach(struct wl_client *client,
		      struct wl_resource *resource,
		      struct wl_resource *buffer_resource,
		      int32_t sx, int32_t sy)
{
	struct remote_surface *surface = get_remote_surface(resource);
	struct wl_buffer *buffer = NULL;

	surface->interface->attach(client, resource, buffer_resource, sx, sy);

	if (buffer_resource)
		buffer = buffer_resource->data;

	remote_surface_attach(surface, buffer, sx, sy);
}

static void
surface_handle_damage(struct wl_client *client,
		      struct wl_resource *resource,
		      int32_t x, int32_t y, int32_t width, int32_t height)
{
	struct remote_surface *surface = get_remote_surface(resource);

	surface->interface->damage(client, resource, x, y, width, height);
}

static void
surface_handle_frame(struct wl_client *client,
		     struct wl_resource *resource, uint32_t callback)
{
	struct remote_surface *surface = get_remote_surface(resource);

	surface->interface->frame(client, resource, callback);
}

static struct wl_region *
get_remote_region(struct remote *remote, pixman_region32_t *region)
{
	struct wl_region *remote_region;
	pixman_box32_t *rectangles;
	int i, n;

	remote_region = wl_compositor_create_region(remote->remote_compositor);
	rectangles = pixman_region32_rectangles(region, &n);
	for (i = 0; i < n; i++)
		wl_region_add(remote_region,
			      rectangles[i].x1, rectangles[i].y1,
			      rectangles[i].x2 - rectangles[i].x1,
			      rectangles[i].y2 - rectangles[i].y1);

	return remote_region;
}

static void
surface_handle_set_opaque_region(struct wl_client *client,
				 struct wl_resource *resource,
				 struct wl_resource *region_resource)
{
	struct remote_surface *surface = get_remote_surface(resource);
	struct weston_region *region = region_resource->data;
	struct wl_region *proxy;

	surface->interface->set_opaque_region(client,
					      resource, region_resource);

	proxy = get_remote_region(surface->remote, &region->region);
	wl_surface_set_opaque_region(surface->proxy, proxy);
	wl_region_destroy(proxy);
}

static void
surface_handle_set_input_region(struct wl_client *client,
				struct wl_resource *resource,
				struct wl_resource *region_resource)
{
	struct remote_surface *surface = get_remote_surface(resource);
	struct weston_region *region = region_resource->data;
	struct wl_region *proxy;

	surface->interface->set_input_region(client,
					     resource, region_resource);

	proxy = get_remote_region(surface->remote, &region->region);
	wl_surface_set_input_region(surface->proxy, proxy);
	wl_region_destroy(proxy);
}

static void
surface_handle_commit(struct wl_client *client,
		      struct wl_resource *resource)
{
	struct remote_surface *surface = get_remote_surface(resource);

	surface->interface->commit(client, resource);

	remote_surface_commit(surface);

	wl_surface_commit(surface->proxy);
}

static const struct wl_surface_interface surface_interface = {
	surface_handle_destroy,
	surface_handle_attach,
	surface_handle_damage,
	surface_handle_frame,
	surface_handle_set_opaque_region,
	surface_handle_set_input_region,
	surface_handle_commit
};


static void
shell_surface_handle_pong(struct wl_client *client,
			  struct wl_resource *resource, uint32_t serial)
{
	struct shell_surface *shell_surface = resource->data;
	struct remote_surface *surface =
		get_remote_surface(&shell_surface->surface->surface.resource);

	if (serial == surface->subst_ping_serial)
		wl_shell_surface_pong(surface->proxy_shell_surface,
				      surface->remote_ping_serial);
	else
		surface->shell_surface_interface->pong(client,
						       resource, serial);
}

static void
shell_surface_handle_move(struct wl_client *client,
			  struct wl_resource *resource,
			  struct wl_resource *seat_resource, uint32_t serial)
{
	struct remote_seat *seat;
	struct shell_surface *shell_surface = resource->data;
	struct remote_surface *surface =
		get_remote_surface(&shell_surface->surface->surface.resource);

	seat = is_remote_seat(seat_resource);
	if (seat) {
		wl_shell_surface_move(surface->proxy_shell_surface,
				      seat->proxy, serial);
	} else {
		surface->shell_surface_interface->move(client, resource,
						       seat_resource, serial);
	}
}

static void
shell_surface_handle_resize(struct wl_client *client,
			    struct wl_resource *resource,
			    struct wl_resource *seat_resource,
			    uint32_t serial, uint32_t edges)
{
	struct remote_seat *seat;
	struct shell_surface *shell_surface = resource->data;
	struct remote_surface *surface =
		get_remote_surface(&shell_surface->surface->surface.resource);

	seat = is_remote_seat(seat_resource);
	if (seat) {
		wl_shell_surface_resize(surface->proxy_shell_surface,
					seat->proxy, serial, edges);
	} else {
		surface->shell_surface_interface->resize(client, resource,
							 seat_resource,
							 serial, edges);
	}
}

static void
shell_surface_handle_set_toplevel(struct wl_client *client,
				  struct wl_resource *resource)
{
}

static void
shell_surface_handle_set_transient(struct wl_client *client,
				   struct wl_resource *resource,
				   struct wl_resource *parent_resource,
				   int x, int y, uint32_t flags)
{
}

static void
shell_surface_handle_set_fullscreen(struct wl_client *client,
				    struct wl_resource *resource,
				    uint32_t method,
				    uint32_t framerate,
				    struct wl_resource *output_resource)
{
	/* FIXME: How could this ever work with different size outputs */
}

static void
shell_surface_handle_set_popup(struct wl_client *client,
			       struct wl_resource *resource,
			       struct wl_resource *seat_resource,
			       uint32_t serial,
			       struct wl_resource *parent_resource,
			       int32_t x, int32_t y, uint32_t flags)
{
}

static void
shell_surface_handle_set_maximized(struct wl_client *client,
				   struct wl_resource *resource,
				   struct wl_resource *output_resource)
{
}

static void
shell_surface_handle_set_title(struct wl_client *client,
			       struct wl_resource *resource, const char *title)
{
}

static void
shell_surface_handle_set_class(struct wl_client *client,
			       struct wl_resource *resource, const char *class)
{
}

static const struct wl_shell_surface_interface shell_surface_implementation = {
	shell_surface_handle_pong,
	shell_surface_handle_move,
	shell_surface_handle_resize,
	shell_surface_handle_set_toplevel,
	shell_surface_handle_set_transient,
	shell_surface_handle_set_fullscreen,
	shell_surface_handle_set_popup,
	shell_surface_handle_set_maximized,
	shell_surface_handle_set_title,
	shell_surface_handle_set_class
};

static struct remote_surface *
remote_surface_create(struct remote *remote, struct weston_surface *surface,
		      struct shell_surface *shell_surface)
{
	struct remote_surface *rs;
	struct wl_region *proxy;

	weston_log("creating remote surface for %p\n", surface);

	rs = malloc(sizeof *rs);
	if (rs == NULL)
		return NULL;

	memset(rs, 0, sizeof *rs);
	rs->remote = remote;
	rs->surface = surface;
	wl_list_init(&rs->stream_block.link);
	rs->proxy = wl_compositor_create_surface(remote->remote_compositor);
	wl_surface_add_listener(rs->proxy, &surface_listener, rs);

	rs->destroy_listener.notify = remote_surface_destroy;
	wl_signal_add(&surface->surface.resource.destroy_signal,
		      &rs->destroy_listener);

	rs->interface = (const struct wl_surface_interface *)
		surface->surface.resource.object.implementation;
	surface->surface.resource.object.implementation =
		(void *) &surface_interface;

	rs->shell_surface = shell_surface;
	if (shell_surface) {
		rs->proxy_shell_surface =
			wl_shell_get_shell_surface(remote->shell, rs->proxy);
		wl_shell_surface_add_listener(rs->proxy_shell_surface,
					      &shell_surface_listener, rs);

		wl_shell_surface_set_toplevel(rs->proxy_shell_surface);

		rs->shell_surface_interface =
			(const struct wl_shell_surface_interface *)
			rs->shell_surface->resource.object.implementation;
		rs->shell_surface->resource.object.implementation =
			(void *) &shell_surface_implementation;
	}

	if (surface->buffer) {
		remote_surface_attach(rs, surface->buffer, 0, 0);
		remote_surface_commit(rs);
	}

	if (pixman_region32_not_empty(&surface->opaque)) {
		proxy = get_remote_region(remote, &surface->opaque);
		wl_surface_set_opaque_region(rs->proxy, proxy);
		wl_region_destroy(proxy);
	}

	if (pixman_region32_not_empty(&surface->input)) {
		proxy = get_remote_region(remote, &surface->input);
		wl_surface_set_input_region(rs->proxy, proxy);
		wl_region_destroy(proxy);
	}

	wl_surface_commit(rs->proxy);
	wl_display_flush(remote->display);

	return rs;
}

static void
remote_binding(struct wl_seat *seat, uint32_t time, uint32_t key, void *data)
{
	struct remote *remote = data;
	struct weston_surface *surface;

	if (seat->keyboard->focus == NULL)
		return;

	/* FIXME: Verify this is a shell surface, handle other surface
	 * types (eg cursor) */

	surface = (struct weston_surface *) seat->keyboard->focus;
	remote_surface_create(remote, surface, surface->private);

#if 0
	wl_list_remove(&surface->layer_link);
	wl_list_insert(&remote->surface_list, &surface->layer_link);
	weston_surface_schedule_repaint(surface);
#endif
}

static void
pointer_handle_set_cursor(struct wl_client *client,
			  struct wl_resource *resource,
			  uint32_t serial,
			  struct wl_resource *surface_resource,
			  int32_t x, int32_t y)
{
	struct remote_seat *seat = resource->data;
	struct weston_surface *surface;
	struct remote_surface *remote_surface;

	if (surface_resource == NULL) {
		wl_pointer_set_cursor(seat->proxy_pointer,
				      seat->enter_serial, NULL, x, y);
		return;
	}

	surface = surface_resource->data;
	remote_surface = get_remote_surface(surface_resource);
	if (remote_surface == NULL)
		remote_surface = remote_surface_create(seat->remote,
						       surface, NULL);

	wl_pointer_set_cursor(seat->proxy_pointer, seat->enter_serial,
			      remote_surface->proxy, x, y);
}

static const struct wl_pointer_interface pointer_implementation = {
	pointer_handle_set_cursor
};

static void
pointer_handle_enter(void *data, struct wl_pointer *pointer,
		     uint32_t serial, struct wl_surface *wl_surface,
		     wl_fixed_t sx, wl_fixed_t sy)
{
	struct remote_seat *seat = data;
	struct remote_surface *surface = wl_surface_get_user_data(wl_surface);

	wl_pointer_set_focus(&seat->pointer,
			     &surface->surface->surface, sx, sy);
}

static void
pointer_handle_leave(void *data, struct wl_pointer *pointer,
		     uint32_t serial, struct wl_surface *surface)
{
	struct remote_seat *seat = data;

	wl_pointer_set_focus(&seat->pointer, NULL, 0, 0);
}

static void
pointer_handle_motion(void *data, struct wl_pointer *pointer,
		      uint32_t time, wl_fixed_t x, wl_fixed_t y)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource = seat->pointer.focus_resource;

	if (resource)
		wl_pointer_send_motion(resource, time, x, y);
}

static void
pointer_handle_button(void *data, struct wl_pointer *pointer, uint32_t serial,
		      uint32_t time, uint32_t button, uint32_t state)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource = seat->pointer.focus_resource;

	/* FIXME: translate serials? */

	if (resource)
		wl_pointer_send_button(resource, serial, time, button, state);
}

static void
pointer_handle_axis(void *data, struct wl_pointer *pointer,
		    uint32_t time, uint32_t axis, wl_fixed_t value)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource = seat->pointer.focus_resource;

	if (resource)
		wl_pointer_send_axis(resource, time, axis, value);
}

static const struct wl_pointer_listener pointer_listener = {
	pointer_handle_enter,
	pointer_handle_leave,
	pointer_handle_motion,
	pointer_handle_button,
	pointer_handle_axis,
};

static void
keyboard_handle_keymap(void *data, struct wl_keyboard *keyboard,
		       uint32_t format, int fd, uint32_t size)
{
	/* We'll never get this event. */
}

static void
keyboard_handle_enter(void *data, struct wl_keyboard *keyboard,
		      uint32_t serial, struct wl_surface *wl_surface,
		      struct wl_array *keys)
{
	struct remote_seat *seat = data;
	struct remote_surface *surface = wl_surface_get_user_data(wl_surface);

	wl_keyboard_set_focus(&seat->keyboard,
			      &surface->surface->surface);
}

static void
keyboard_handle_leave(void *data, struct wl_keyboard *keyboard,
		      uint32_t serial, struct wl_surface *surface)
{
	struct remote_seat *seat = data;

	wl_keyboard_set_focus(&seat->keyboard, NULL);
}

static void
keyboard_handle_key(void *data, struct wl_keyboard *keyboard,
		    uint32_t serial, uint32_t time, uint32_t key,
		    uint32_t state)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource = seat->keyboard.focus_resource;

	if (resource)
		wl_keyboard_send_key(resource, serial, time, key, state);
}

static void
keyboard_handle_modifiers(void *data, struct wl_keyboard *keyboard,
			  uint32_t serial, uint32_t mods_depressed,
			  uint32_t mods_latched, uint32_t mods_locked,
			  uint32_t group)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource = seat->keyboard.focus_resource;

	if (resource)
		wl_keyboard_send_modifiers(resource, serial, mods_depressed,
					   mods_latched, mods_locked, group);
}

static const struct wl_keyboard_listener keyboard_listener = {
	keyboard_handle_keymap,
	keyboard_handle_enter,
	keyboard_handle_leave,
	keyboard_handle_key,
	keyboard_handle_modifiers,
};

static void
seat_handle_capabilities(void *data, struct wl_seat *wl_seat,
			 enum wl_seat_capability capabilities)
{
	struct remote_seat *seat = data;

	if (capabilities & WL_SEAT_CAPABILITY_POINTER) {
		seat->proxy_pointer = wl_seat_get_pointer(seat->proxy);
		wl_pointer_add_listener(seat->proxy_pointer,
					&pointer_listener, seat);

		wl_pointer_init(&seat->pointer);
		wl_seat_set_pointer(&seat->seat, &seat->pointer);

	} else {
		wl_pointer_destroy(seat->proxy_pointer);
		seat->proxy_pointer = NULL;
	}

	if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD) {
		seat->proxy_keyboard = wl_seat_get_keyboard(seat->proxy);
		wl_keyboard_add_listener(seat->proxy_keyboard,
					 &keyboard_listener, seat);

		wl_keyboard_init(&seat->keyboard);
		wl_seat_set_keyboard(&seat->seat, &seat->keyboard);
	} else {
		wl_keyboard_destroy(seat->proxy_keyboard);
		seat->proxy_keyboard = NULL;
	}
}

static const struct wl_seat_listener seat_listener = {
	seat_handle_capabilities,
};

static void unbind_resource(struct wl_resource *resource)
{
	wl_list_remove(&resource->link);
	free(resource);
}

static void
remote_seat_get_pointer(struct wl_client *client,
			struct wl_resource *resource, uint32_t id)
{
	struct remote_seat *seat = resource->data;
	struct wl_resource *cr;

	if (!seat->proxy_pointer)
		return;

        cr = wl_client_add_object(client, &wl_pointer_interface,
				  &pointer_implementation, id, seat);
	wl_list_insert(&seat->pointer.resource_list, &cr->link);
	cr->destroy = unbind_resource;
}

static void
remote_seat_get_keyboard(struct wl_client *client,
			 struct wl_resource *resource, uint32_t id)
{
	struct remote_seat *seat = resource->data;
	struct wl_resource *cr;

	if (!seat->proxy_keyboard)
		return;

        cr = wl_client_add_object(client, &wl_keyboard_interface,
				  NULL, id, seat);
	wl_list_insert(&seat->keyboard.resource_list, &cr->link);
	cr->destroy = unbind_resource;

	wl_keyboard_send_keymap(cr, WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1,
				seat->keymap_fd,
				seat->keymap_size);

	if (seat->seat.keyboard->focus &&
	    seat->seat.keyboard->focus->resource.client == client) {
		wl_keyboard_set_focus(seat->seat.keyboard,
				      seat->seat.keyboard->focus);
	}
}

static void
remote_seat_get_touch(struct wl_client *client,
		      struct wl_resource *resource, uint32_t id)
{
}

static const struct wl_seat_interface remote_seat_interface = {
	remote_seat_get_pointer,
	remote_seat_get_keyboard,
	remote_seat_get_touch,
};

static struct remote_seat *
is_remote_seat(struct wl_resource *resource)
{
	if (resource->object.implementation == (void *) &remote_seat_interface)
		return resource->data;
	else
		return NULL;
}

static void
bind_seat(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
	struct remote_seat *seat = data;
	struct wl_resource *resource;
	enum wl_seat_capability caps = 0;

	resource = wl_client_add_object(client, &wl_seat_interface,
					&remote_seat_interface, id, seat);
	wl_list_insert(&seat->seat.base_resource_list, &resource->link);
	resource->destroy = unbind_resource;

	if (seat->proxy_pointer)
		caps |= WL_SEAT_CAPABILITY_POINTER;
	if (seat->proxy_keyboard)
		caps |= WL_SEAT_CAPABILITY_KEYBOARD;

	wl_seat_send_capabilities(resource, caps);
}

static void
remote_add_seat(struct remote *remote, uint32_t id)
{
	struct remote_seat *seat;

	seat = malloc(sizeof *seat);
	if (seat == NULL)
		return;

	memset(seat, 0, sizeof *seat);

	seat->remote = remote;
	seat->proxy = wl_registry_bind(remote->registry, id,
				       &wl_seat_interface, 1);
	wl_seat_add_listener(seat->proxy, &seat_listener, seat);

	wl_seat_init(&seat->seat);
	wl_display_add_global(seat->remote->compositor->wl_display,
			      &wl_seat_interface, seat, bind_seat);
}

static void
output_handle_geometry(void *data,
		       struct wl_output *wl_output,
		       int32_t x, int32_t y,
		       int32_t physical_width,
		       int32_t physical_height,
		       int subpixel,
		       const char *make,
		       const char *model,
		       int transform)
{
	struct remote_output *output = data;

	/* Discard x and y. */
	output->output.make = strdup(make);
	output->output.model = strdup(model);
	output->output.mm_width = physical_width;
	output->output.mm_height = physical_height;
	output->output.subpixel = subpixel;
	output->output.transform = transform;
	wl_list_init(&output->output.mode_list);
}

struct remote_mode {
	struct weston_mode base;
};

static void
output_handle_mode(void *data,
		   struct wl_output *wl_output,
		   uint32_t flags,
		   int width,
		   int height,
		   int refresh)
{
	struct remote_output *output = data;
	struct remote_mode *mode;

	mode = malloc(sizeof *mode);

	mode->base.flags = flags;
	mode->base.width = width;
	mode->base.height = height;
	mode->base.refresh = refresh;

	if (flags & WL_OUTPUT_MODE_CURRENT)
		output->output.current = &mode->base;

	wl_list_insert(output->output.mode_list.prev, &mode->base.link);
}

static const struct wl_output_listener output_listener = {
	output_handle_geometry,
	output_handle_mode
};

static void
output_callback_done(void *data, struct wl_callback *callback, uint32_t time)
{
	struct remote_output *output = data;
	int x = 0, y = 0, width = 800, height = 600;

	wl_callback_destroy(callback);
	weston_output_init(&output->output, output->remote->compositor,
			   x, y, width, height, WL_OUTPUT_TRANSFORM_NORMAL);
}

static const struct wl_callback_listener output_callback_listener = {
	output_callback_done
};

static void
remote_output_destroy(struct weston_output *output)
{
	free(output->make);
	free(output->model);
	weston_output_destroy(output);
	wl_list_remove(&output->link);
}

static void
remote_output_repaint(struct weston_output *output_base,
		      pixman_region32_t *damage)
{
}

static void
remote_add_output(struct remote *remote, uint32_t id)
{
	struct remote_output *output;
	struct wl_callback *callback;

	output = malloc(sizeof *output);
	if (output == NULL)
		return;

	memset(output, 0, sizeof *output);
	output->remote = remote;
	output->proxy = wl_registry_bind(remote->registry, id,
					 &wl_output_interface, 1);

	output->output.destroy = remote_output_destroy;
	output->output.repaint = remote_output_repaint;

	wl_output_add_listener(output->proxy, &output_listener, output);

	callback = wl_display_sync(remote->display);
	wl_callback_add_listener(callback, &output_callback_listener, output);
}

static void
remote_handle_keymap(void *data, struct remote *proxy,
		     struct wl_keyboard *keyboard, uint32_t format,
		     uint32_t tag, uint32_t size)
{
	struct remote *remote = data;
	struct remote_seat *seat = wl_keyboard_get_user_data(keyboard);

	remote_stream_handle_tag(remote->stream, tag,
				 keymap_tag_handler, seat);
}

static const struct remote_listener remote_listener = {
	remote_handle_keymap
};

static void
registry_handle_global(void *data, struct wl_registry *registry, uint32_t name,
		       const char *interface, uint32_t version)
{
	struct remote *remote = data;

	if (strcmp(interface, "wl_compositor") == 0) {
		remote->remote_compositor =
			wl_registry_bind(registry, name,
					 &wl_compositor_interface, 1);
	} else if (strcmp(interface, "wl_output") == 0) {
		remote_add_output(remote, name);
	} else if (strcmp(interface, "wl_seat") == 0) {
		remote_add_seat(remote, name);
	} else if (strcmp(interface, "wl_shell") == 0) {
		remote->shell = wl_registry_bind(registry, name,
						 &wl_shell_interface, 1);
	} else if (strcmp(interface, "remote") == 0) {
		remote->proxy_remote =
			wl_registry_bind(registry, name, &remote_interface, 1);
		remote_add_listener(remote->proxy_remote,
				    &remote_listener, remote);
	}
}

static const struct wl_registry_listener registry_listener = {
	registry_handle_global
};

static void
destroy_remote(struct wl_listener *listener, void *data)
{
	struct remote *remote =
		container_of(listener, struct remote, destroy_listener);

	/* FIXME: More clean up needed, unhook all forwarded surfaces,
	 * for example. */

	wl_registry_destroy(remote->registry);
	wl_event_source_remove(remote->stream->source);
	close(remote->stream->fd);
	wl_event_source_remove(remote->source);
	wl_event_source_remove(remote->protocol_source);
	close(remote->protocol_fd[0]);
	wl_display_disconnect(remote->display);
	wl_list_remove(&remote->destroy_listener.link);
	free(remote);
}

static int
remote_handle_event(int fd, uint32_t mask, void *data)
{
	struct remote *remote = data;
	int count = 0;

	if (mask & (WL_EVENT_HANGUP | WL_EVENT_ERROR)) {
		destroy_remote(&remote->destroy_listener, NULL);
		return 0;
	}

	if (mask & WL_EVENT_READABLE)
		count = wl_display_dispatch(remote->display);
	if (mask & WL_EVENT_WRITABLE)
		wl_display_flush(remote->display);

	if (mask == 0) {
		count = wl_display_dispatch_pending(remote->display);
		wl_display_flush(remote->display);
	}

	return count;
}

WL_EXPORT int
module_init(struct weston_compositor *compositor)
{
	struct remote *remote;
	struct wl_event_loop *loop =
		wl_display_get_event_loop(compositor->wl_display);
	int port = 2222;
	static const char *host = "localhost";

	remote = malloc(sizeof *remote);
	if (remote == NULL)
		return -1;

	memset(remote, 0, sizeof *remote);
	remote->compositor = compositor;
	remote->stream = remote_stream_connect(remote, host, port);
	if (remote->stream == NULL)
 		return -1;

	if (os_socketpair_cloexec(AF_UNIX, SOCK_STREAM, 0,
				  remote->protocol_fd) < 0)
		return -1;

	remote->protocol_source =
		wl_event_loop_add_fd(loop, remote->protocol_fd[0],
				     WL_EVENT_READABLE,
				     protocol_data, remote);

	remote->display =
		wl_display_connect_to_fd(remote->protocol_fd[1]);

	if (remote->display == NULL) {
		weston_log("failed to connect to remote display on %s:%d\n",
			   host, port);
		free(remote);
		return -1;
	}

	remote->source = wl_event_loop_add_fd(loop, remote->protocol_fd[1],
					      WL_EVENT_READABLE,
					      remote_handle_event, remote);
	if (remote->source == NULL) {
		wl_display_disconnect(remote->display);
		free(remote);
		return -1;
	}

	wl_event_source_check(remote->source);

	remote->registry = wl_display_get_registry(remote->display);
	wl_registry_add_listener(remote->registry, &registry_listener, remote);

	weston_compositor_add_key_binding(compositor, KEY_F, MODIFIER_SUPER,
					  remote_binding, remote);

	wl_list_init(&remote->surface_list);
	remote->destroy_listener.notify = destroy_remote;
	wl_signal_add(&compositor->destroy_signal, &remote->destroy_listener);

	weston_log("connected to remote host %s:%d\n", host, port);

	wl_display_flush(remote->display);

	return 0;
}
