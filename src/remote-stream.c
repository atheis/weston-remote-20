/*
 * Copyright © 2012 Intel Corporation
 *
 * Permission to use, copy, modify, distribute, and sell this software and
 * its documentation for any purpose is hereby granted without fee, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of the copyright holders not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.  The copyright holders make
 * no representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 * SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/uio.h>

#include "compositor.h"
#include "remote-stream.h"

int
remote_stream_write(struct remote_stream *stream)
{
	struct remote_stream_block *block, *next;
	uint32_t header;
	struct iovec iov[2];
	int len;

	wl_list_for_each_safe(block, next, &stream->block_queue, link) {
		/* FIXME: Coalesce all this into one writev? */
		if (block->written == 0) {
			header = (block->tag << 24) | block->size;
			iov[0].iov_base = &header;
			iov[0].iov_len = sizeof header;
			iov[1].iov_base = block->data;
			iov[1].iov_len = block->size;

			/* FIXME: async write this... */
			len = writev(stream->fd, iov, 2);
			if (len == -1)
				return len;

			assert(len >= (int) sizeof header);
			len -= sizeof header;
		} else {
			len = write(stream->fd, block->data + block->written,
				    block->size - block->written);
		}

		if (len < 0)
			weston_log("write error: %m\n");

		block->written += len;

		if (block->written < block->size)
			break;

		wl_list_remove(&block->link);
		wl_list_init(&block->link);
 	}
 
	stream->tag = 1;

	return 1;
}

int
remote_stream_read(struct remote_stream *stream)
{
	size_t remain;
	uint32_t tag;

	if (stream->handler == NULL) {
		if (stream->header == 0)
			/* FIXME: Check this read... */
			read(stream->fd,
			     &stream->header, sizeof stream->header);
		tag = stream->header >> 24;
		if (tag == 0)
			stream->tag_handler_count = 1;
		if (tag >= stream->tag_handler_count)
			/* If there's no handler for this tag yet,
			 * return to the main loop to process protocol
			 * events first. */
			return 1;

		stream->len = 0;
		stream->total = stream->header & 0xffffff;
		stream->handler = &stream->tag_handler[tag];
		stream->header = 0;
	} else {
		remain = stream->total - stream->len;
		stream->handler->func(stream, remain, stream->handler->data);
		if (stream->len == stream->total)
			stream->handler = NULL;
	}

	return 1;
}

void
remote_stream_handle_tag(struct remote_stream *stream, uint32_t tag,
			 remote_stream_handler_func_t func, void *data)
{
	/* FIXME: Bad assert */
	assert(tag < ARRAY_LENGTH(stream->tag_handler));

	if (tag != stream->tag_handler_count)
		assert("tags out of sync");
	stream->tag_handler[tag].func = func;
	stream->tag_handler[tag].data = data;
	stream->tag_handler_count++;
}

void
remote_stream_queue_block(struct remote_stream *stream,
			  struct remote_stream_block *block,
			  void *data, size_t size)
{
	if (!wl_list_empty(&block->link))
		return;

	wl_list_insert(stream->block_queue.prev, &block->link);
	block->tag = stream->tag++;
	block->written = 0;
	block->data = data;
	block->size = size;
}
